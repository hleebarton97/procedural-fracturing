using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Another Indie Studio
// Lee Barton
public class ProceduralFracturing : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public int FractureCascades {
        get { return this.fractureCascades; }
        set { this.fractureCascades = value; }
    }

    public float SlicePointRadius {
        get { return this.randomSlicePointRadius; }
        set { this.randomSlicePointRadius = value; }
    }

    public bool AddForceOnFracture {
        get { return this.addForceOnFracture; }
        set { this.addForceOnFracture = value; }
    }

    public float Force {
        get { return this.force; }
        set { this.force = value; }
    }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private int fractureCascades = 1;
    [SerializeField] private float randomSlicePointRadius = 1.0f;
    [SerializeField] private bool addForceOnFracture = false;
    [SerializeField] private float force = 0.0f;

    private bool edgeIsSet = false;
    private Vector3 edgeVertex = Vector3.zero;
    private Vector2 edgeUV = Vector2.zero;
    private Plane edgePlane = new Plane();

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void OnDrawGizmosSelected () {
        Gizmos.DrawWireSphere(
            this.transform.localPosition,
            this.randomSlicePointRadius
        );
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void Fracture () {
        Debug.Log("Start fracturing...");

        // Get current mesh state
        Mesh originalMesh = GetComponent<MeshFilter>().mesh;
        originalMesh.RecalculateBounds();

        List<NewMesh> newMeshParts = new List<NewMesh>(); // New meshes after fracture
        List<NewMesh> newMeshSubParts = new List<NewMesh>();

        // Create 'copy' of original mesh with our mesh part
        NewMesh originalNewMesh = new NewMesh(
            originalMesh.vertices,
            originalMesh.normals,
            new int[originalMesh.subMeshCount][],
            originalMesh.uv,
            originalMesh.bounds
        );

        for (int i = 0; i < originalMesh.subMeshCount; i++) {
            originalNewMesh.SetTrianglesAtIndex(
                i,
                originalMesh.GetTriangles(i)
            );
        }
        newMeshParts.Add(originalNewMesh);

        // For every slice to be made,
        // iterate through each mesh piece and create
        // a plane to 'slice' the mesh
        for (int i = 0; i < this.fractureCascades; i++) {
            for (int j = 0; j < newMeshParts.Count; j++) {
                Bounds bounds = newMeshParts[j].Bounds;
                bounds.Expand(0.5f);

                // Make slice with plane
                Vector3 randSpherePoint = Random.onUnitSphere * this.randomSlicePointRadius;
                Vector3 position = new Vector3(
                        Random.Range(bounds.min.x, bounds.max.x),
                        Random.Range(bounds.min.y, bounds.max.y),
                        Random.Range(bounds.min.z, bounds.max.z)
                    );
                Plane slicingPlane =  new Plane(
                    randSpherePoint,
                    position
                );

                newMeshSubParts.Add(this.GenerateNewMesh(newMeshParts[j], slicingPlane, true));
                newMeshSubParts.Add(this.GenerateNewMesh(newMeshParts[j], slicingPlane, false));
            }
            newMeshParts = new List<NewMesh>(newMeshSubParts);
            newMeshSubParts.Clear();
        }

        for (int i = 0; i < newMeshParts.Count; i++) {
            NewMesh newMesh = newMeshParts[i];
            newMesh.MakeGameObject(this);
            if (this.addForceOnFracture) {
                newMesh.GameObject.GetComponent<Rigidbody>()
                    .AddForceAtPosition(newMesh.Bounds.center * this.force, this.transform.position);
            }
        }

        Destroy(this.gameObject);

        Debug.Log("Fractured!");
    }

    //
    private NewMesh GenerateNewMesh (NewMesh originalNewMesh, Plane plane, bool isPositive) {
        NewMesh newMesh = new NewMesh();
        Ray rayA = new Ray();
        Ray rayB = new Ray();

        for (int i = 0; i < originalNewMesh.Triangles.Length; i++) {
            int[] triangles = originalNewMesh.Triangles[i];
            this.edgeIsSet = false;

            // Check for triangles that have all sides on positive axis
            for (int j = 0; j < triangles.Length; j = j + 3) {
                bool sideA = plane.GetSide(originalNewMesh.Vertices[triangles[j]]) == isPositive;
                bool sideB = plane.GetSide(originalNewMesh.Vertices[triangles[j + 1]]) == isPositive;
                bool sideC = plane.GetSide(originalNewMesh.Vertices[triangles[j + 2]]) == isPositive;

                int sideCount = (sideA ? 1 : 0) +
                                (sideB ? 1 : 0) +
                                (sideC ? 1 : 0);

                // Skip triangle that has no points on same side
                if (sideCount == 0) {
                    continue;
                } // Add triangle to new mesh as all points are on same side
                else if (sideCount == 3) {
                    newMesh.AddTriangle(
                        i,
                        originalNewMesh.Vertices[triangles[j]],
                        originalNewMesh.Vertices[triangles[j + 1]],
                        originalNewMesh.Vertices[triangles[j + 2]],
                        originalNewMesh.Normals[triangles[j]],
                        originalNewMesh.Normals[triangles[j + 1]],
                        originalNewMesh.Normals[triangles[j + 2]],
                        originalNewMesh.UV[triangles[j]],
                        originalNewMesh.UV[triangles[j + 1]],
                        originalNewMesh.UV[triangles[j + 2]]
                    );
                    continue;
                }

                // Otherwise, calculate cutting points
                int sameSideIndex = 2;
                if (sideB == sideC) { sameSideIndex = 0; }
                else if (sideA == sideC) { sameSideIndex = 1; }

                rayA.origin = originalNewMesh.Vertices[triangles[j + sameSideIndex]];
                Vector3 directionA = originalNewMesh.Vertices[triangles[j + ((sameSideIndex + 1) % 3)]] - originalNewMesh.Vertices[triangles[j + sameSideIndex]];
                rayA.direction = directionA;
                plane.Raycast(rayA, out float enterA);
                float lerpA = enterA / directionA.magnitude;

                rayB.origin = originalNewMesh.Vertices[triangles[j + sameSideIndex]];
                Vector3 directionB = originalNewMesh.Vertices[triangles[j + ((sameSideIndex + 2) % 3)]] - originalNewMesh.Vertices[triangles[j + sameSideIndex]];
                rayB.direction = directionB;
                plane.Raycast(rayB, out float enterB);
                float lerpB = enterB / directionB.magnitude;

                // Add edge to mesh
                this.AddEdgeToNewMesh(
                    i,
                    newMesh,
                    isPositive ? plane.normal * -1f : plane.normal,
                    rayA.origin + rayA.direction.normalized * enterA,
                    rayB.origin + rayB.direction.normalized * enterB,
                    Vector2.Lerp(originalNewMesh.UV[triangles[j + sameSideIndex]], originalNewMesh.UV[triangles[j + ((sameSideIndex + 1) % 3)]], lerpA),
                    Vector2.Lerp(originalNewMesh.UV[triangles[j + sameSideIndex]], originalNewMesh.UV[triangles[j + ((sameSideIndex + 2) % 3)]], lerpB)
                );

                if (sideCount == 1) {
                    newMesh.AddTriangle(
                        i,
                        originalNewMesh.Vertices[triangles[j + sameSideIndex]],
                        rayA.origin + rayA.direction.normalized * enterA,
                        rayB.origin + rayB.direction.normalized * enterB,
                        originalNewMesh.Normals[triangles[j + sameSideIndex]],
                        Vector3.Lerp(originalNewMesh.Normals[triangles[j + sameSideIndex]], originalNewMesh.Normals[triangles[j + ((sameSideIndex + 1) % 3)]], lerpA),
                        Vector3.Lerp(originalNewMesh.Normals[triangles[j + sameSideIndex]], originalNewMesh.Normals[triangles[j + ((sameSideIndex + 2) % 3)]], lerpA),
                        originalNewMesh.UV[triangles[j + sameSideIndex]],
                        Vector2.Lerp(originalNewMesh.UV[triangles[j + sameSideIndex]], originalNewMesh.UV[triangles[j + ((sameSideIndex + 1) % 3)]], lerpA),
                        Vector2.Lerp(originalNewMesh.UV[triangles[j + sameSideIndex]], originalNewMesh.UV[triangles[j + ((sameSideIndex + 2) % 3)]], lerpB)
                    );
                    continue;
                } else if (sideCount == 2) {
                    newMesh.AddTriangle(
                        i,
                        rayA.origin + rayA.direction.normalized * enterA,
                        originalNewMesh.Vertices[triangles[j + ((sameSideIndex + 1) % 3)]],
                        originalNewMesh.Vertices[triangles[j + ((sameSideIndex + 2) % 3)]],
                        Vector3.Lerp(originalNewMesh.Normals[triangles[j + sameSideIndex]], originalNewMesh.Normals[triangles[j + ((sameSideIndex + 1) % 3)]], lerpA),
                        originalNewMesh.Normals[triangles[j + ((sameSideIndex + 1) % 3)]],
                        originalNewMesh.Normals[triangles[j + ((sameSideIndex + 2) % 3)]],
                        Vector2.Lerp(originalNewMesh.UV[triangles[j + sameSideIndex]], originalNewMesh.UV[triangles[j + ((sameSideIndex + 1) % 3)]], lerpA),
                        originalNewMesh.UV[triangles[j + ((sameSideIndex + 1) % 3)]],
                        originalNewMesh.UV[triangles[j + ((sameSideIndex + 2) % 3)]]
                    );
                    newMesh.AddTriangle(
                        i,
                        rayA.origin + rayA.direction.normalized * enterA,
                        originalNewMesh.Vertices[triangles[j + ((sameSideIndex + 2) % 3)]],
                        rayB.origin + rayB.direction.normalized * enterB,
                        Vector3.Lerp(originalNewMesh.Normals[triangles[j + sameSideIndex]], originalNewMesh.Normals[triangles[j + ((sameSideIndex + 1) % 3)]], lerpA),
                        originalNewMesh.Normals[triangles[j + ((sameSideIndex + 2) % 3)]],
                        Vector3.Lerp(originalNewMesh.Normals[triangles[j + sameSideIndex]], originalNewMesh.Normals[triangles[j + ((sameSideIndex + 2) % 3)]], lerpB),
                        Vector2.Lerp(originalNewMesh.UV[triangles[j + sameSideIndex]], originalNewMesh.UV[triangles[j + ((sameSideIndex + 1) % 3)]], lerpA),
                        originalNewMesh.UV[triangles[j + ((sameSideIndex + 2) % 3)]],
                        Vector2.Lerp(originalNewMesh.UV[triangles[j + sameSideIndex]], originalNewMesh.UV[triangles[j + ((sameSideIndex + 2) % 3)]], lerpB)
                    );
                    continue;
                }
            }
        }
        newMesh.FillArrays();
        return newMesh;
    }

    // Add edge to mesh
    private void AddEdgeToNewMesh(
        int subMesh,
        NewMesh newMesh,
        Vector3 normal,
        Vector3 vertexA,
        Vector3 vertexB,
        Vector2 uvA,
        Vector2 uvB
    ) {
        if (!this.edgeIsSet) {
            this.edgeIsSet = true;
            this.edgeVertex = vertexA;
            this.edgeUV = uvA;
        } else {
            this.edgePlane.Set3Points(this.edgeVertex, vertexA, vertexB);
            newMesh.AddTriangle(
                subMesh,
                this.edgeVertex,
                this.edgePlane.GetSide(this.edgeVertex + normal) ? vertexA : vertexB,
                this.edgePlane.GetSide(this.edgeVertex + normal) ? vertexB : vertexA,
                normal,
                normal,
                normal,
                this.edgeUV,
                uvA,
                uvB
            );
        }
    }
}
