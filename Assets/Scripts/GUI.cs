using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

// Lee Barton
public class GUI : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private ProceduralFracturing _ProceduralFracturing;

    [SerializeField] private TextMeshProUGUI fracturesTM;
    [SerializeField] private Slider fracturesSlider;

    [SerializeField] private TextMeshProUGUI pointRadiusTM;
    [SerializeField] private Slider pointRadiusSlider;

    [SerializeField] private Toggle forceToggle;

    [SerializeField] private TextMeshProUGUI forceTM;
    [SerializeField] private Slider forceSlider;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.fracturesSlider.value = 5;
        this.pointRadiusSlider.value = 0.5f;
        this.forceSlider.value = 1500;
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void UpdateFracturesValue () {
        this.fracturesTM.text = this.fracturesSlider.value.ToString();
        this._ProceduralFracturing.FractureCascades = (int)this.fracturesSlider.value;
    }

    public void UpdatePointRadiusValue () {
        this.pointRadiusTM.text = Math.Round(this.pointRadiusSlider.value, 2).ToString();
        this._ProceduralFracturing.SlicePointRadius = this.pointRadiusSlider.value;
    }

    public void UpdateForceToggle () {
        this._ProceduralFracturing.AddForceOnFracture = this.forceToggle.isOn;
    }

    public void UpdateForceValue () {
        this.forceTM.text = this.forceSlider.value.ToString();
        this._ProceduralFracturing.Force = this.forceSlider.value;
    }

    public void Reset () {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
