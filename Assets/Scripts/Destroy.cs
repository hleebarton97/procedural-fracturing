using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Another Indie Studio
// Lee Barton
public class Destroy : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private ProceduralFracturing _ProceduralFracturing;
    //private Destructable _Destructable;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this._ProceduralFracturing = this.GetComponent<ProceduralFracturing>();
        //this._Destructable = this.GetComponent<Destructable>();
    }

    private void OnMouseOver () {
        if (Input.GetButtonDown("Fire1")) {
            Debug.Log("Destroy");
            this._ProceduralFracturing.Fracture();
            //this._Destructable.DestroyObject();
        }
    }

}
