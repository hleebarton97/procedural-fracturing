using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Another Indie Studio
// Lee Barton
public class Destructable : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject defaultVersion;
    [SerializeField] private GameObject brokenVersion;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.brokenVersion.SetActive(false);
        this.defaultVersion.SetActive(true);
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void DestroyObject () {
        this.defaultVersion.SetActive(false);
        this.brokenVersion.SetActive(true);
    }
}
