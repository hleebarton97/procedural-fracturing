using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Another Indie Studio
// Lee Barton
public class NewMesh {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public Vector3[] Vertices { get { return this.vertices; } }
    public Vector3[] Normals { get { return this.normals; } }
    public int[][] Triangles { get { return this.triangles; } }
    public Vector2[] UV { get { return this.uv; } }
    public GameObject GameObject { get { return this.obj; } }
    public Bounds Bounds { get { return this.bounds; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private Vector3[] vertices;
    private Vector3[] normals;
    private int[][] triangles;
    private Vector2[] uv;
    private GameObject obj;
    private Bounds bounds = new Bounds();

    private List<Vector3> verticesList = new List<Vector3>();
    private List<Vector3> normalsList = new List<Vector3>();
    private List<List<int>> trianglesList = new List<List<int>>();
    private List<Vector2> uvList = new List<Vector2>();

    /////////////////////////////////////////////////////////////////
    // C O N S T R U C T O R
    /////////////////////////////////////////////////////////////////

    public NewMesh () {}

    public NewMesh (
        Vector3[] vertices,
        Vector3[] normals,
        int[][] triangles,
        Vector2[] uv,
        Bounds bounds
    ) {
        this.vertices = vertices;
        this.normals = normals;
        this.triangles = triangles;
        this.uv = uv;
        this.bounds = bounds;
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void AddTriangle (
        int subMesh,
        Vector3 vertexA,
        Vector3 vertexB,
        Vector3 vertexC,
        Vector3 normalA,
        Vector3 normalB,
        Vector3 normalC,
        Vector2 uvA,
        Vector2 uvB,
        Vector2 uvC
    ) {
        if (this.trianglesList.Count - 1 < subMesh) {
            this.trianglesList.Add(new List<int>());
        }

        this.trianglesList[subMesh].Add(this.verticesList.Count);
        this.verticesList.Add(vertexA);
        
        this.trianglesList[subMesh].Add(this.verticesList.Count);
        this.verticesList.Add(vertexB);

        this.trianglesList[subMesh].Add(this.verticesList.Count);
        this.verticesList.Add(vertexC);

        this.normalsList.Add(normalA);
        this.normalsList.Add(normalB);
        this.normalsList.Add(normalC);

        this.uvList.Add(uvA);
        this.uvList.Add(uvB);
        this.uvList.Add(uvC);

        this.bounds.min = Vector3.Min(Bounds.min, vertexA);
        this.bounds.min = Vector3.Min(Bounds.min, vertexB);
        this.bounds.min = Vector3.Min(Bounds.min, vertexC);
        this.bounds.max = Vector3.Min(Bounds.max, vertexA);
        this.bounds.max = Vector3.Min(Bounds.max, vertexB);
        this.bounds.max = Vector3.Min(Bounds.max, vertexC);
    }

    public void FillArrays () {
        this.vertices = this.verticesList.ToArray();
        this.normals = this.normalsList.ToArray();
        this.uv = this.uvList.ToArray();
        this.triangles = new int[this.trianglesList.Count][];

        for (int i = 0; i < this.trianglesList.Count; i++) {
            this.triangles[i] = this.trianglesList[i].ToArray();
        }
    }

    public void MakeGameObject (ProceduralFracturing original) {
        this.obj = new GameObject(original.name);
        this.obj.transform.position = original.transform.position;
        this.obj.transform.rotation = original.transform.rotation;
        this.obj.transform.localScale = original.transform.localScale;

        Mesh mesh = new Mesh();
        mesh.name = original.GetComponent<MeshFilter>().mesh.name;
        mesh.vertices = this.vertices;
        mesh.normals = this.normals;
        mesh.uv = this.uv;
        for (int i = 0; i < this.triangles.Length; i++) {
            mesh.SetTriangles(this.triangles[i], i, true);
        }
        this.bounds = mesh.bounds;

        MeshRenderer meshRenderer = this.obj.AddComponent<MeshRenderer>();
        meshRenderer.materials = original.GetComponent<MeshRenderer>().materials;

        MeshFilter meshFilter = this.obj.AddComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        MeshCollider meshCollider = this.obj.AddComponent<MeshCollider>();
        meshCollider.convex = true;

        this.obj.AddComponent<Rigidbody>();

        ProceduralFracturing _ProceduralFracturing = this.obj.AddComponent<ProceduralFracturing>();
        _ProceduralFracturing.FractureCascades = original.FractureCascades;
        _ProceduralFracturing.AddForceOnFracture = original.AddForceOnFracture;
        _ProceduralFracturing.Force = original.Force;

        this.obj.AddComponent<Destroy>();
    }

    /////////////////////////////////////////////////////////////////
    // S E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void SetTrianglesAtIndex (int i, int[] triangles) {
        this.triangles[i] = triangles;
    }

    /////////////////////////////////////////////////////////////////
    // G E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

}
